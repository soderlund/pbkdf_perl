package PBKDF;

use strict;
use warnings;
use utf8;
use open qw(:std :encoding(UTF-8));

use Carp ();
use POSIX ();

## no critic qw(ProhibitMagicNumbers)
my $_DKLEN_MAX = (2 ** 32 - 1);
## use critic

sub pbkdf2
{
## no critic qw(ProhibitMagicNumbers)
	6 == scalar(@_)
## use critic
		or Carp::croak(q(argc));
	my($p, $s, $c, $dklen, $prf, $hlen) = @_;
	defined($p)
		or Carp::croak(q(p));
	defined($s)
		or Carp::croak(q(s));
	(defined($c)
		&& $c =~ m/^\-?\d+$/aams
		&& $c > 0)
		or Carp::croak(q(c));
	(defined($dklen)
		&& $dklen =~ m/^\-?\d+$/aams
		&& $dklen > 0)
		or Carp::croak(q(dklen));
	defined($prf)
		or Carp::croak(q(prf));
	(defined($hlen)
		&& $hlen =~ m/^\-?\d+$/aams
		&& $hlen > 0)
		or Carp::croak(q(hlen));
	$dklen <= $_DKLEN_MAX * $hlen
		or Carp::croak(q(derived key too long));
	my $l = POSIX::ceil($dklen / $hlen);
	#
	# `$r` is unused:
	#
	# my($r) = $dklen - ($l - 1) * $hlen;
	#
	my $dk = q();
	for my $i (1 .. $l)
	{
		my $u_c = &{$prf}(
			$s . pack(q(N), $i),
			$p
		);
		my $t_l = $u_c;
		for my $j (1 .. ($c - 1))
		{
			#
			# Note that in Perl, `hmac_sha512($data, $key)`,
			# but in RFC 2898 PBKDF2, `prf($key, $data)`.
			#
			$u_c = &{$prf}($u_c, $p);
			$t_l ^= $u_c;
		}
		$dk .= $t_l;
	}
	return(substr($dk, 0, $dklen));
}

sub pbkdf2_hex
{
	return(unpack(q(H*), pbkdf2(@_)));
}

1;

__END__

=encoding utf8

=for stopwords PBKDF PBKDF2

=head1 NAME

PBKDF - password-based key derivation function.

=head1 SYNOPSIS

=over

=item C<pbkdf2 EXPR EXPR EXPR EXPR EXPR EXPR>

=item C<pbkdf2_hex EXPR EXPR EXPR EXPR EXPR EXPR>

=back

=head1 DESCRIPTION

=head2 C<pbkdf2 EXPR EXPR EXPR EXPR EXPR EXPR>

Password-based key derivation function PBKDF2 as specified by RFC 2898.
Arguments:

=over

=item Password. (Not validated but must be defined.)

=item Salt. (Not validated but must be defined.)

=item Iteration count. Must be C<E<gt> 0>.

=item Derived key length. Must be C<E<gt> 0>.

=item Pseudo-random function. For example C<Digest::SHA::hmac_sha512>.

=item Length of pseudo-random function output. Must be C<E<gt> 0>.

=back

Returns a binary key.

=head2 C<pbkdf2_hex EXPR EXPR EXPR EXPR EXPR EXPR>

Same as C<pbkdf2()> but returns the key as a hexadecimal string.

=head1 SEE ALSO

B<Digest::SHA>(3perl),
B<RFC 2898> (C<http://www.ietf.org/rfc/rfc2898.txt>)

=head1 AUTHOR

Alexander Söderlund

=head1 COPYRIGHT

Copyright © 2015, 2018 Alexander Söderlund, Sweden

Permission to use, copy, modify, and distribute this software for any
purpose with or without fee is hereby granted, provided that the above
copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

=cut

