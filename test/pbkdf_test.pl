#!/usr/bin/perl

use strict;
use warnings;
use utf8;
use open qw(:std :encoding(UTF-8));

use Carp ();
use Digest::SHA ();

use PBKDF ();

sub test_pbkdf2
{
## no critic qw(ProhibitMagicNumbers)
	scalar(@_) >= 7
## use critic
		or Carp::croak(q(argc));
	my($p, $s, $c, $dklen, $prf, $hlen, $hash_expect, $skip) = @_;
	$hash_expect = join(q(), split(q( ), $hash_expect));
	if ($skip)
	{
		warn(qq(SKIP: $hash_expect\n));
		return();
	}
	my($hash_actual) = PBKDF::pbkdf2_hex(
		$p,
		$s,
		$c,
		$dklen,
		$prf,
		$hlen
	);
	if ($hash_expect eq $hash_actual)
	{
		print(qq(OK: $hash_expect\n))
			or die(qq(Failed to print\n));
	}
	else
	{
		die(qq(ERROR: $hash_expect != $hash_actual\n));
	}
	return();
}

test_pbkdf2(
	q(password),
	q(salt),
	1,
## no critic qw(ProhibitMagicNumbers)
	20,
## use critic
	\&Digest::SHA::hmac_sha1,
## no critic qw(ProhibitMagicNumbers)
	20,
## use critic
	q(0c 60 c8 0f 96 1f 0e 71 f3 a9 b5 24 af 60 12 06 2f e0 37 a6)
);

test_pbkdf2(
	q(password),
	q(salt),
	2,
## no critic qw(ProhibitMagicNumbers)
	20,
## use critic
	\&Digest::SHA::hmac_sha1,
## no critic qw(ProhibitMagicNumbers)
	20,
## use critic
	q(ea 6c 01 4d c7 2d 6f 8c cd 1e d9 2a ce 1d 41 f0 d8 de 89 57)
);

test_pbkdf2(
	q(password),
	q(salt),
## no critic qw(ProhibitMagicNumbers)
	4096,
	20,
## use critic
	\&Digest::SHA::hmac_sha1,
## no critic qw(ProhibitMagicNumbers)
	20,
## use critic
	q(4b 00 79 01 b7 65 48 9a be ad 49 d9 26 f7 21 d0 65 a4 29 c1)
);

test_pbkdf2(
	q(password),
	q(salt),
## no critic qw(ProhibitMagicNumbers)
	16_777_216,
	20,
## use critic
	\&Digest::SHA::hmac_sha1,
## no critic qw(ProhibitMagicNumbers)
	20,
## use critic
	q(ee fe 3d 61 cd 4d a4 e4 e9 94 5b 3d 6b a2 15 8c 26 34 e9 84),
	1	# Skip this one because it takes too long.
);

test_pbkdf2(
	q(passwordPASSWORDpassword),
	q(saltSALTsaltSALTsaltSALTsaltSALTsalt),
## no critic qw(ProhibitMagicNumbers)
	4096,
	25,
## use critic
	\&Digest::SHA::hmac_sha1,
## no critic qw(ProhibitMagicNumbers)
	20,
## use critic
	q(3d 2e ec 4f e4 1c 84 9b 80 c8 d8 36 62 c0 e4 4a 8b 29 1a 96)
	. q( 4c f2 f0 70 38)
);

test_pbkdf2(
	qq(pass\0word),
	qq(sa\0lt),
## no critic qw(ProhibitMagicNumbers)
	4096,
	16,
## use critic
	\&Digest::SHA::hmac_sha1,
## no critic qw(ProhibitMagicNumbers)
	20,
## use critic
	q(56 fa 6a a7 55 48 09 9d cc 37 d7 f0 34 25 e0 c3)
);

exit(0);

__END__

# pbkdf2 (RFC 6070: http://www.ietf.org/rfc/rfc6070.txt)

=encoding utf8

Copyright © 2015, 2018 Alexander Söderlund, Sweden

Permission to use, copy, modify, and distribute this software for any
purpose with or without fee is hereby granted, provided that the above
copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

=cut

