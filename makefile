.POSIX:

PRGNAM = pbkdf_perl
VERSION = 181110

LINT = perlcritic
LINTFLAGS = -1 --noprofile\
	--exclude ProhibitHardTabs\
	--exclude ProhibitParensWithBuiltins\
	--exclude RequireArgUnpacking\
	--exclude RequireExtendedFormatting\
	--exclude RequirePodSections\
	--exclude RequireRcsKeywords\
	--exclude RequireTidyCode\
	--exclude RequireVersionVar

SRCDIR = src
TESTDIR = test
TESTBIN = $(TESTDIR)/pbkdf_test.pl

all:

clean:

mrproper: clean

dist: mrproper
	mkdir '$(PRGNAM)-$(VERSION)'
	find . -maxdepth 1 -mindepth 1\
		-and -not -name '$(PRGNAM)-$(VERSION)'\
		-and -not -name '.git'\
		-exec cp -r '{}' '$(PRGNAM)-$(VERSION)' \;
	tar -cf '$(PRGNAM)-$(VERSION).tar' '$(PRGNAM)-$(VERSION)'
	gzip -9 '$(PRGNAM)-$(VERSION).tar'
	rm -r '$(PRGNAM)-$(VERSION)'

lint:
	$(LINT) $(LINTFLAGS) $(SRCDIR) $(TESTDIR)

test: FORCE
	env PERL5LIB='$(SRCDIR)' $(TESTBIN)

FORCE:

